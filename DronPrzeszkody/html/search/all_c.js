var searchData=
[
  ['pokazos_5fox',['PokazOs_OX',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a11421d7c67deab6b7524cc492407e897',1,'PzG::LaczeDoGNUPlota::PokazOs_OX(bool Pokaz)'],['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#ae112972af57167c3b053bf922bce6bbf',1,'PzG::LaczeDoGNUPlota::PokazOs_OX() const']]],
  ['pokazos_5foy',['PokazOs_OY',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a7c3db909b266fc30808e86406c04b516',1,'PzG::LaczeDoGNUPlota::PokazOs_OY(bool Pokaz)'],['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a7298f469f6932f5c808dcf620650b4b8',1,'PzG::LaczeDoGNUPlota::PokazOs_OY() const']]],
  ['pokazos_5foz',['PokazOs_OZ',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a9fabfe88cb1801a5de8923f45f514b99',1,'PzG::LaczeDoGNUPlota::PokazOs_OZ(bool Pokaz)'],['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a22c708af33c57bf3b5d1b4e82b4017b7',1,'PzG::LaczeDoGNUPlota::PokazOs_OZ() const']]],
  ['prostopadloscian',['Prostopadloscian',['../class_prostopadloscian.html',1,'']]],
  ['prostopadloscian_2ehh',['Prostopadloscian.hh',['../_prostopadloscian_8hh.html',1,'']]],
  ['przeslijdognuplota',['PrzeslijDoGNUPlota',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a5063854b7232a7951d120a21df63f2b7',1,'PzG::LaczeDoGNUPlota']]],
  ['przesun',['Przesun',['../class_figura_geometryczna.html#aad35835677a3cf163bfb94dff8453c4e',1,'FiguraGeometryczna']]],
  ['przeszkodapr',['PrzeszkodaPr',['../class_przeszkoda_pr.html',1,'']]],
  ['przeszkodapr_2ehh',['PrzeszkodaPr.hh',['../_przeszkoda_pr_8hh.html',1,'']]],
  ['punkt',['Punkt',['../class_figura_geometryczna.html#a2d0bea311d45c148936633119de561c6',1,'FiguraGeometryczna']]],
  ['punkt_5fglobalny',['Punkt_globalny',['../class_figura_geometryczna.html#ad67a3a97173d879372944e0aefd64c5e',1,'FiguraGeometryczna']]],
  ['pzg',['PzG',['../namespace_pz_g.html',1,'']]]
];
