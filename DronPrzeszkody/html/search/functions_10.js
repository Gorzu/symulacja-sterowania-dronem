var searchData=
[
  ['zapis',['Zapis',['../class_figura_geometryczna.html#ae86561d4cc3b38f98a643b22f6c4bcc3',1,'FiguraGeometryczna']]],
  ['zapisz',['Zapisz',['../class_dron.html#afbf73dce01fc688b0b4d0014d57dcbc6',1,'Dron']]],
  ['zapiszustawienierotacjiiskali',['ZapiszUstawienieRotacjiISkali',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#aa92b463e8cbae31b50dd797a4183bce8',1,'PzG::LaczeDoGNUPlota']]],
  ['zapiszustawieniezakresu',['ZapiszUstawienieZakresu',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a4579aecf7b4777fdde0cae4e98c275c2',1,'PzG::LaczeDoGNUPlota']]],
  ['zmax',['Zmax',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a20a5d03e1fc19c682032bffc54340f12',1,'PzG::LaczeDoGNUPlota']]],
  ['zmiendrona',['ZmienDrona',['../class_scena.html#a5c903f5445d6750911ea4182d8161905',1,'Scena']]],
  ['zmiennazwepliku',['ZmienNazwePliku',['../class_pz_g_1_1_info_pliku_do_rysowania.html#ae734c69f5cecf9c0584e3a7f433340ea',1,'PzG::InfoPlikuDoRysowania']]],
  ['zmientrybrys',['ZmienTrybRys',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a10950349b348fd3a3d4143e95337527c',1,'PzG::LaczeDoGNUPlota']]],
  ['zmin',['Zmin',['../class_pz_g_1_1_lacze_do_g_n_u_plota.html#a9068bd9a9873ba9c6d70016f1ae7cd7f',1,'PzG::LaczeDoGNUPlota']]],
  ['zwrocdrona',['ZwrocDrona',['../class_scena.html#a4c7bfb936ea9e8ebfd7bafbbb60c32d5',1,'Scena']]],
  ['zwrociloscwektorow',['ZwrocIloscWektorow',['../class_wektor.html#a3cd1385dfa3d738c93679358fba81c01',1,'Wektor']]],
  ['zwroclacznailoscwektorow',['ZwrocLacznaIloscWektorow',['../class_wektor.html#a9ee79389dfef2a5cc8a376e128820d76',1,'Wektor']]],
  ['zwrocnrdrona',['ZwrocNrDrona',['../class_scena.html#aae64e2b5d52e9e508837e6d6675a84f3',1,'Scena']]],
  ['zwrocsrodek',['ZwrocSrodek',['../class_obiekt_sceny.html#ace7dbdb0508d767dd7a6ec1086d6723c',1,'ObiektSceny']]]
];
