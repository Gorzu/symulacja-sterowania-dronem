var searchData=
[
  ['obrot',['Obrot',['../class_dron.html#ab951fe581599c4c1c87e1bcb45f67f9e',1,'Dron::Obrot()'],['../class_figura_geometryczna.html#a09387b2b43ff7b8f64314bc4b943446b',1,'FiguraGeometryczna::Obrot()']]],
  ['obrotsmigiel',['ObrotSmigiel',['../class_graniastoslup.html#a69f4ea107154f2ea27a60e92af694726',1,'Graniastoslup']]],
  ['operator_28_29',['operator()',['../class_macierz.html#ab90c30e59034965754917aa75d03d349',1,'Macierz::operator()(int Wiersz, int Kolumna) const'],['../class_macierz.html#a452a5143b5273569481fba2cd515b604',1,'Macierz::operator()(int Wiersz, int Kolumna)']]],
  ['operator_2a',['operator*',['../class_macierz.html#a1a74d4e0ed76ba1645149ed181fab02c',1,'Macierz::operator*(Wektor&lt; Wymiar &gt; const &amp;Arg2) const'],['../class_macierz.html#a72ef1f33903228712cb808ab468f76b2',1,'Macierz::operator*(Macierz&lt; Wymiar &gt; const &amp;Arg2)']]],
  ['operator_2b',['operator+',['../class_graniastoslup.html#a5c3318283465d1a5f7e6238fcea3a6b7',1,'Graniastoslup::operator+()'],['../class_prostopadloscian.html#afd4c755e0b039104ab336094e1b3b4ca',1,'Prostopadloscian::operator+()'],['../class_wektor.html#a4f15151622e4a6677ddc2c7407941d04',1,'Wektor::operator+()']]],
  ['operator_2b_3d',['operator+=',['../class_wektor.html#a7b321d411e304d492914b3bad446e3cb',1,'Wektor']]],
  ['operator_2d',['operator-',['../class_wektor.html#a55321819bf46da6f3fd2928b32ebb26d',1,'Wektor']]],
  ['operator_2f',['operator/',['../class_wektor.html#a08abc2c2c98a2c02b411ce4fa8fe4f83',1,'Wektor']]],
  ['operator_3c_3c',['operator&lt;&lt;',['../_macierz_8hh.html#ac3a210e69b75f6e7fe48a36a48982ddc',1,'operator&lt;&lt;(std::ostream &amp;Strm, const Macierz&lt; Wymiar &gt; &amp;Mac):&#160;Macierz.hh'],['../_wektor_8hh.html#a15a4e77c646218f10d5e34a3fd4a877a',1,'operator&lt;&lt;(std::ostream &amp;Strm, const Wektor&lt; Wymiar &gt; &amp;Wek):&#160;Wektor.hh']]],
  ['operator_3e_3e',['operator&gt;&gt;',['../_wektor_8hh.html#a936d42e42728823fb375e5e2122c2fe0',1,'Wektor.hh']]],
  ['operator_5b_5d',['operator[]',['../class_prostopadloscian.html#a3181376ccb435c01920eb388e13db1fa',1,'Prostopadloscian::operator[]()'],['../class_wektor.html#af1f133a89fcdf7561573d08027a6d05a',1,'Wektor::operator[](int Ind) const'],['../class_wektor.html#a81e448591dd6356f0e88bcfc9bad99c9',1,'Wektor::operator[](int Ind)']]]
];
