var indexSectionsWithContent =
{
  0: "_abcdfgiklmoprstuwxyz~",
  1: "dfgilmopsw",
  2: "p",
  3: "dfglmopsw",
  4: "abcdiklmoprsuwxyz~",
  5: "_kpsw",
  6: "mw",
  7: "rt",
  8: "rt",
  9: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros"
};

