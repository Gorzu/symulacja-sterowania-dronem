#ifndef MACIERZ3X3_HH
#define MACIERZ3X3_HH

#include "Macierz.hh"

/*!
 * \brief instancja szablonu Macierz<>
 */
typedef Macierz<3> Macierz3x3;

Macierz3x3 MacierzX(double kat);

Macierz3x3 MacierzY(double kat);

Macierz3x3 MacierzZ(double kat);


#endif
