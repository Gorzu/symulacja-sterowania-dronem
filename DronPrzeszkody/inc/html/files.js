var files =
[
    [ "Dron.hh", "_dron_8hh.html", "_dron_8hh" ],
    [ "Fabryka.hh", "_fabryka_8hh.html", "_fabryka_8hh" ],
    [ "FiguraGeometryczna.hh", "_figura_geometryczna_8hh.html", [
      [ "FiguraGeometryczna", "class_figura_geometryczna.html", "class_figura_geometryczna" ]
    ] ],
    [ "Graniastoslup.hh", "_graniastoslup_8hh.html", "_graniastoslup_8hh" ],
    [ "lacze_do_gnuplota.hh", "lacze__do__gnuplota_8hh.html", "lacze__do__gnuplota_8hh" ],
    [ "Macierz.hh", "_macierz_8hh.html", "_macierz_8hh" ],
    [ "Macierz3x3.hh", "_macierz3x3_8hh.html", "_macierz3x3_8hh" ],
    [ "ObiektSceny.hh", "_obiekt_sceny_8hh.html", [
      [ "ObiektSceny", "class_obiekt_sceny.html", "class_obiekt_sceny" ]
    ] ],
    [ "Obsluga.hh", "_obsluga_8hh.html", "_obsluga_8hh" ],
    [ "Prostopadloscian.hh", "_prostopadloscian_8hh.html", [
      [ "Prostopadloscian", "class_prostopadloscian.html", "class_prostopadloscian" ]
    ] ],
    [ "PrzeszkodaPr.hh", "_przeszkoda_pr_8hh.html", [
      [ "PrzeszkodaPr", "class_przeszkoda_pr.html", "class_przeszkoda_pr" ]
    ] ],
    [ "Scena.hh", "_scena_8hh.html", "_scena_8hh" ],
    [ "Wektor.hh", "_wektor_8hh.html", "_wektor_8hh" ],
    [ "Wektor3D.hh", "_wektor3_d_8hh.html", "_wektor3_d_8hh" ]
];