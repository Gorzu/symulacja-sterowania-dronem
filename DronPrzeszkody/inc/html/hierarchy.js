var hierarchy =
[
    [ "Fabryka", "class_fabryka.html", null ],
    [ "FiguraGeometryczna", "class_figura_geometryczna.html", [
      [ "Graniastoslup", "class_graniastoslup.html", null ],
      [ "Prostopadloscian", "class_prostopadloscian.html", null ],
      [ "PrzeszkodaPr", "class_przeszkoda_pr.html", null ]
    ] ],
    [ "PzG::InfoPlikuDoRysowania", "class_pz_g_1_1_info_pliku_do_rysowania.html", null ],
    [ "PzG::LaczeDoGNUPlota", "class_pz_g_1_1_lacze_do_g_n_u_plota.html", null ],
    [ "Macierz< Wymiar >", "class_macierz.html", null ],
    [ "ObiektSceny", "class_obiekt_sceny.html", [
      [ "Dron", "class_dron.html", null ],
      [ "PrzeszkodaPr", "class_przeszkoda_pr.html", null ]
    ] ],
    [ "Scena", "class_scena.html", null ],
    [ "Wektor< Wymiar >", "class_wektor.html", null ],
    [ "Wektor3D", "class_wektor3_d.html", null ],
    [ "Wektor< 3 >", "class_wektor.html", null ]
];