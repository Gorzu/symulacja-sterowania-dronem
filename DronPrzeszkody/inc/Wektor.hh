#ifndef WEKTOR_HH
#define WEKTOR_HH

#include <assert.h>
#include <cmath>
#include <iomanip>
#include <iostream>

/*!
 * \file
 * \brief Plik zawiera definicje szablonu klasy Wektor<>
 */

/*
 * \brief Szablon klasy Wektor<>
 * Szablon klasy Wektor<> pozwala na stworzenie wektora w dowolnym wymiarze.
 */
template <int Wymiar>
class Wektor
{
  /*!
   * \brief Pole opisuje wspolrzedne poszczegolnych skladowych wektora
   *
   * Wielosc tablicy zależy od wymiaru wektora.
   */
  double Wsp[Wymiar];

  static int IloscWektorow;
  
  static int LacznaIlosc;  // ilość stworzonych obiektów klasy Wektor3D

 public:
 
  /*
   *\brief Konstruktor domyslny
   *
   * Zerowanie składowych
   */
  Wektor()
  {
    for (int i = 0; i < Wymiar; i++)
    {
      this->Wsp[i] = 0;
    }
    ++IloscWektorow;
    ++LacznaIlosc;
  }
  

  /*
   *\brief Konstruktor kopiujacy
   * 
   */

  Wektor(const Wektor &obj)
  {
    ++IloscWektorow;
    ++LacznaIlosc;
    for (int i = 0; i < Wymiar; ++i)
    {
      Wsp[i] = obj[i];
    }
  }
  

  /*
   *\brief Konstruktor parametryczny
   *
   */
  Wektor(int x, int y, int z)
  {
    this->Wsp[0] = x;
    this->Wsp[1] = y;
    this->Wsp[2] = z;
    ++IloscWektorow;
    ++LacznaIlosc;
  }
  

  /*
   *\brief Destruktor obiektu Wektor
   *
   */
  ~Wektor(){ --IloscWektorow; }




  static int ZwrocIloscWektorow()
  {
    return IloscWektorow; 
  }

  static int ZwrocLacznaIloscWektorow()
  {
    return LacznaIlosc;
  }
  


  /*
   * \brief Przeciazenie operatora [] dla Wektor<>
   *
   */
  double operator[](int Ind) const
  {
    assert(Ind >= 0 && Ind < Wymiar);
    return this->Wsp[Ind];
  }
 

  /*
   * \brief Przeciazenie operatora [] dla Wektor<> 
   *
   * Zmienianie pojedynczej współrzędnej
   */
  double &operator[](int Ind)
  {
    assert(Ind >= 0 && Ind < Wymiar);
    return this->Wsp[Ind];
  }
  
  /*
   *\brief Obliczanie długości wektora
   *
   */
  double Dlugosc()
  {
    double suma = 0;
    for (int i = 0; i < Wymiar; i++)
    {
      suma += (Wsp[i] * Wsp[i]);
    }
    return sqrt(suma);
  }
  


  /*!
   *\brief Przeciążenia operatorów arytmetycznych dla wektorów
   *
   */
  Wektor<Wymiar> operator+(Wektor<Wymiar> Arg2) const
  {
    Wektor<Wymiar> temp;
    for (int i = 0; i < Wymiar; i++) {
      temp[i] = this->Wsp[i] + Arg2[i];
    }
    return temp;
  }


  Wektor<Wymiar> &operator+= (const Wektor<Wymiar> &Arg2)
  {
    for (int i = 0; i < Wymiar; i++) this->Wsp[i] += Arg2[i];
    return *this;
  }


  Wektor<Wymiar> operator-(Wektor<Wymiar> Arg2) const
  {
    Wektor<Wymiar> temp;
    for (int i = 0; i < Wymiar; i++) {
      temp[i] = this->Wsp[i] - Arg2[i];
    }
    return temp;
  }


  Wektor<Wymiar> operator/(int Arg2) const
  {
    Wektor<Wymiar> temp;
    for (int i = 0; i < Wymiar; i++)
    {
      temp[i] = this->Wsp[i] / Arg2;
    }
    return temp;
  }
};

template <int Wymiar>
int Wektor<Wymiar>::IloscWektorow = 0;

template <int Wymiar>
int Wektor<Wymiar>::LacznaIlosc = 0;

/*
 *\brief Przeciazenie dla operatora >> dla klasy Wektor<>
 *
 * Wypisanie współrzędnych wektora
 */
template <int Wymiar>
inline std::istream &operator>>(std::istream &Strm, Wektor<Wymiar> &Wek)
{
  for (int i = 0; i < Wymiar; i++) {
    Strm >> Wek[i];
    if (Strm.fail()) return Strm;
  }
  return Strm;
}

/*
 * \brief Przeciazenie operatora << dla klasy Wektor<>
 *
 */
template <int Wymiar>
inline std::ostream &operator<<(std::ostream &Strm, const Wektor<Wymiar> &Wek)
{
  Strm << "[ ";
  for (int i = 0; i < Wymiar; i++) {
    Strm << Wek[i] << " ";
  }
  Strm << "]";
  return Strm;
}

#endif
