#ifndef WEKTOR3D_HH
#define WEKTOR3D_HH

#include "Wektor.hh"

/*!
 * \class Wektor3D
 *
 *\brief Klasa Wektor3D jest instancja szablonu klasy Wektor<>.
 */
typedef Wektor<3> Wektor3D;

#endif
