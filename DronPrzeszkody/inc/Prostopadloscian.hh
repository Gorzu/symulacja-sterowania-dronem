#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH
#include "FiguraGeometryczna.hh"


/*!
 *\brief Klasa modelująca prostopadłościan
 */

class Prostopadloscian : public FiguraGeometryczna
{
 public:
  /*
   *\brief Metoda inicjaliuzuje wierzcholki prostopadloscianu do zadanych wartości
   * 
   */
  void Inicjalizuj();
  


  /*
   *\brief Przeciazenie operatora+ 
   *
   *pozwalaja na przesunięcie prostopadłościanu o zadany wektor
   */
  
  Prostopadloscian operator+(Wektor3D Arg2);
 

  /*
   *\brief Przeciazenie operatora[]
   * 
   * Pozwala na dostep do współrzędnych wierzchołków
   */
 
  Wektor3D operator[](int Ind)
  { 
    return Punkt[Ind];
  }

};
#endif