#ifndef MACIERZ_HH
#define MACIERZ_HH

#include <cmath>
#include "Wektor3D.hh"


/*!
 * \brief Szablon klasy Macierz<>
 *
 * Umożliwia tworzenie macierzy o dowolnych wymiarach
 */


template <int Wymiar>
class Macierz
{
  double Tab[Wymiar][Wymiar];

 public:
  /*
   *\brief Konstruktor tworzący macierz jednostkową
   */
  Macierz()
  {
    for (int i = 0; i < Wymiar; i++)
    {
      for (int j = 0; j < Wymiar; j++)
      {
        this->Tab[i][j] = 0;
      }
    }
  }
  

  Macierz<3> kat_x(const double kat_x)
  {
    Tab[0][0] = 1;
    Tab[0][1] = 0;
    Tab[0][2] = 0;

    Tab[1][0] = 0;
    Tab[1][1] = cos (kat_x * M_PI / 180);
    Tab[1][2] = sin (kat_x * M_PI / 180) * (-1);

    Tab[2][0] = 0;
    Tab[2][1] = sin (kat_x * M_PI / 180);
    Tab[2][2] = cos (kat_x * M_PI / 180);

    return (*this);
  }


   Macierz<3> kat_y(const double kat_y)
  {
       Tab[0][0] = cos (kat_y * M_PI / 180);
       Tab[0][1] = 0;
       Tab[0][2] = sin (kat_y * M_PI / 180);

       Tab[1][0] = 0;
       Tab[1][1] = 1;
       Tab[1][2] = 0;

       Tab[2][0] = sin (kat_y * M_PI / 180) * (-1);
       Tab[2][1] = 0;
       Tab[2][2] = cos (kat_y * M_PI / 180);

      return (*this);
  }
  

   void kat_z(const double kat_z)
    {
       Tab[0][0] = cos (kat_z * M_PI / 180);
       Tab[0][1] = sin (kat_z * M_PI / 180) * (-1);
       Tab[0][2] = 0;

       Tab[1][0] = sin (kat_z * M_PI / 180);
       Tab[1][1] = cos (kat_z * M_PI / 180);
       Tab[1][2] = 0;

       Tab[2][0] = 0;
       Tab[2][1] = 0;
       Tab[2][2] = 1;
    }  


  /*!
   * \brief Przeciazenie operatora() dla klasy Macierz<>
   *
   */
  

  double operator()(int Wiersz, int Kolumna) const
  {
    assert(Wiersz >= 0 && Wiersz < Wymiar && Kolumna >= 0 && Kolumna < Wymiar);
    return this->Tab[Wiersz][Kolumna];
  }
 
  double &operator()(int Wiersz, int Kolumna)
  {
    assert(Wiersz >= 0 && Wiersz < Wymiar && Kolumna >= 0 && Kolumna < Wymiar);
    return this->Tab[Wiersz][Kolumna];
  }
  



  /*!
   * \brief Przeciazenie operatora* dla klasy Macierz<>
   */
  Wektor<Wymiar> operator* (Wektor<Wymiar> const &Arg2) const
  {
    Wektor<Wymiar> tmp;
    double suma;
    for (int i = 0; i < Wymiar; i++)
    {
      for (int j = 0; j < Wymiar; j++)
      {
        suma = 0;
        for (int k = 0; k < Wymiar; k++)
        {
          suma += (*this)(i, k) * Arg2[k];
        }
        tmp[i] = suma;
      }
    }
    return tmp;
  }
  


  /*!
   *\brief Przeciazenie operatora * dla mnozenia macierz razy macierz
   *
   */
  Macierz<Wymiar> operator*(Macierz<Wymiar> const &Arg2)
  {
    Macierz<Wymiar> tmp;
    double suma;
    for (int i = 0; i < Wymiar; i++)
    {
      for (int j = 0; j < Wymiar; j++)
      {
        suma = 0;
        for (int k = 0; k < Wymiar; k++)
        {
          suma += (*this)(i, k) * Arg2(k, j);
        }
        tmp(i, j) = suma;
      }
    }
    return tmp;
  }
};




/*!
 *\brief Przeciazenie operatora << dla klasy Macierz<>
 *
 */


template <int Wymiar>
inline std::ostream &operator<<(std::ostream &Strm, const Macierz<Wymiar> &Mac)
{
  Strm << std::endl;
  for (int i = 0; i < Wymiar; i++)
  {
    for (int j = 0; j < Wymiar; j++)
    {
      Strm << std::setw(20) << Mac(i, j) << " ";
    }
    Strm << std::endl;
  }
  Strm << std::endl;
  return Strm;
}

#endif
