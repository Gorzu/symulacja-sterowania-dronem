#pragma once

#include "Wektor3D.hh"

/*!
*\brief Klasa modelująca obiekt sceny
* Zastosowanie np. do wykrywania kolizji
*/

class ObiektSceny
{
protected:

    Wektor3D srodek = Wektor3D();
    Wektor3D _polozenie;
        
public:

    Wektor3D ZwrocSrodek() 
    {
        return srodek;
    }



    /*
   *  \brief Konstruktor bezparametryczny. 
   */
    ObiektSceny(){};

   /*
   *  \brief Konstruktor kopiujacy.
   */
 	ObiektSceny(const ObiektSceny &obiekt){};
 	
 	/*
 	* \ Destruktor.
 	*/
 	~ObiektSceny(){};

  
  /*
   *  \brief Wirtualna metoda injalizująca prostopadłościany.
   */
virtual void Incjalizuj(int numerobiektu){}


   /*
   *  \brief Wirtualna metoda injalizująca obiekty z okreslonymi parametrami.
   */
 virtual void Incjalizuj2(int numerobiektu, double x, double y, double z){}

};