#pragma once

#include "Dron.hh"
#include "ObiektSceny.hh"
#include "Fabryka.hh"
#include <memory>

#define LICZBA_DRONOW 2
#define LICZBA_PRZESZKOD 3

/*!
 *\brief Klasa modelująca pojęcie sceny
 */
class Scena
{
 
  Dron* aktualny;
  int indeksWybranego=0;

PzG::LaczeDoGNUPlota  Lacze;
std::list<std::shared_ptr<Dron>> drony; // lista dronów
std::list<std::shared_ptr<Dron>>::iterator itdron; // iterator na listę dronów
std::list<std::shared_ptr<ObiektSceny>> obiekty; // lista obiektów sceny
std::list<std::shared_ptr<ObiektSceny>>::iterator itobiekt; // iterator na listę obiektów


 public:
 
 /*
 *\brief Metoda Inicjalizująca scenę (rozmieszcza drony oraz przeszkody w układzie współrzędnych)
 */
 void Inicjalizuj(PzG::LaczeDoGNUPlota& Lacze);
 
/*
 *\brief Wskaźnik zwraca aktualnie użytkowanego drona
 */
 Dron* ZwrocDrona();
 
/*
 *\brief Metoda odpowiedzialna za wybór drona do sterowania
 */
 void ZmienDrona();
 
/*
 *\brief Metoda wyświetlająca współrzędne wszystkich dronów
 */
 void WyswietlDrony();
 
/*
 *\brief Metoda wyświetlająca numery wszystkich dronów
 */
 int ZwrocNrDrona();

/*
*\brief Metoda tworząca nowego drona.
*/
 void DodajDrona();

/*
*\brief Metoda usuwająca wybranego drona.
*/
 void UsunDrona();

/*
*\brief Metoda tworząca nową przeszkodę.
*/
 void DodajPrzeszkode();

/*
*\brief Metoda usuwająca wybraną przeszkodę
*/
 void UsunPrzeszkode();


 };