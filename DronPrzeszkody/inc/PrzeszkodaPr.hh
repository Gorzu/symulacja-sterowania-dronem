#pragma once

#include "FiguraGeometryczna.hh"
#include "ObiektSceny.hh"


/*!
*\brief Klasa modelująca pojęcie prostopadłościennej przeszkody
*/
class PrzeszkodaPr : public FiguraGeometryczna, public ObiektSceny
{


public:

  /*
   * \brief Metoda odpowiedzialna za inicjalizację przeszkody
   */
  void Inicjalizuj(double wysokosc, double szerokosc, double dlugosc);

};


