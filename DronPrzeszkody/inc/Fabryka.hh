#pragma once

#include "Wektor3D.hh"
#include "ObiektSceny.hh"
#include "Dron.hh"
#include "PrzeszkodaPr.hh"
#include <memory>


PzG::LaczeDoGNUPlota Lacze;


enum TYP{ Dron, Przeszkoda };

/*!
 * \brief Klasa modelująca pojęcie Fabryki.
 * 
 */

class Fabryka
{
   private:

/*
*\brief konstruktor bezparametryczny - Fabryka
*/
    Fabryka(){}

/*
*\biref konstruktor kopiujący - Fabryka
*/
    Fabryka(const Fabryka&) = delete; 

/*
*\brien destruktor - fabryka
*/
    ~Fabryka(){} 
    
  /*
  *\brief Statystyczna instancja - Fabryka
  */
    static Fabryka *wFabryka;
  public:


     /*
   *  \brief Tworzenie statycznej fabryki.
   */
   static Fabryka* StworzFabryke()
    {
      if (wFabryka) return wFabryka;
      return wFabryka = new Fabryka();
    }
    

    /*
   *  \brief Usuwanie statycznej fabryki.
   */
   static void UsunFabryke()
    {
      if (wFabryka) delete wFabryka;   
    }



/*
*\brief wskaźnik na zainicjalizowany wskaźnik klasy PrzeszkodaPr
*/
std::shared_ptr<ObiektySceny> StworzPrzeszkode(double X,double Y, double Z, Wektor3D Trans, int numer) const;

/*
*\brief wskaźnik na zainicjalizowany wskaźnik klasy PrzeszkodaPr lub Dron
*/
std::shared_ptr<ObiektySceny> Stworz(TYP TYP, int numer, Wektor3D Trans);



};
