#pragma once

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include "Wektor3D.hh"
#include "Macierz3x3.hh"

/*!
*\brief klasa modelująca pojęcie figury geometrycznej
*/

class FiguraGeometryczna
{
protected:

	std::vector<Wektor3D> Punkt;
    std::vector<Wektor3D> Punkt_globalny;
	Wektor3D Wektor_Trans;
	double katObrotu = 0;

public:

/*
*\brief Metoda odpowiedzialna za zapis współrzędnych figury geometrycznej do pliku
*/
    void Zapis(std::ostream &Strm);

/*
*\brief Metoda odpowiedzialna za przemieszczenie figury geometrycznej
*/
	void Przesun(Wektor3D wektor);

/*
*\brief Metoda odpowiedzialna za rotację figury geometrycznej
*/
	void Obrot(double kat);
};
