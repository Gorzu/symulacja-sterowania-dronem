#pragma once

#include <unistd.h>
#include "Graniastoslup.hh"
#include "lacze_do_gnuplota.hh"
#include "PrzeszkodaPr.hh"

#define LICZBA_ROTOROW 4



/*!
 * \brief Klasa modelująca drona
 *
 */

class Dron : public ObiektSceny
{
 
  Graniastoslup Rotor[LICZBA_ROTOROW];
  
  Prostopadloscian Korpus;
 
  double Kat_Obrotu = 0;

 public:
  /*!
   * \brief Metody odpowiedzialne za animacje ruchu drona
   *
   */
  void Ruch(PzG::LaczeDoGNUPlota Lacze, int numerDrona);

  void Cofanie(PzG::LaczeDoGNUPlota Lacze, int numerDrona);

  void Obrot(PzG::LaczeDoGNUPlota Lacze, int numerDrona);
  
  /*
   * \brief Metoda odpowiedzialna za zapis współrzędnych drona
   */
  void Zapisz(int  numerDrona);
  /*
   * \brief Metoda odpowiedzialna za inicjalizację obiektów, z których zbudowany jest dron
   */
  void Inicjalizuj();
  

  /*
   * \brief Metody odpowiedzialne za animację rotorów
   */
  void Animacja(PzG::LaczeDoGNUPlota Lacze, int numerDrona);

  void AktualizujRotory();

/*
* \brief Metoda odpowiedzialna za ustawienie wybranego drona w trójwymiarowym układzie współrzędnych
*/
  void Ustaw(PzG::LaczeDoGNUPlota& Lacze, Wektor3D wek, int numerDrona);

};