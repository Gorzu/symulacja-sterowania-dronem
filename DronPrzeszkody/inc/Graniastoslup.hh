#pragma once
#include "Prostopadloscian.hh"


#define LICZBA_ROTOROW 4


/*!
 *\brief Klasa modeluje pojecie Graniastoslupa
 */

class Graniastoslup : public FiguraGeometryczna
{

  Wektor3D srodek_drona;

 public:
  /*
   * \brief Inicjalizacja graniastosłupa o zadanych współrzędnych wierzchołków
   */
  void Inicjalizuj();
  
  /*
   * \brief Startowe ustawienie drona
   */
  void UstawPolozenie(Wektor3D srodek_drona);

/*
*\brief MEtoda odpowiedzialna za rotację rotorów drona
*/
  void ObrotSmigiel(double kat);

/*
*\brief Metoda ustawiająca środek drona, przydatna do realizacji innych metod
*/
  void UstawSrodekDrona(Wektor3D Arg);
  /*
   * \brief Przeciazenie odpowiedzialne za dodawanie wektora do współrzędnych wierzchołków
   *
   * Przesuwanie drona o zadany wektor
   */
  Graniastoslup operator+(Wektor3D Arg2);
};
