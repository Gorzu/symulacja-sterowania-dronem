#include <iostream>
#include <iomanip>
#include <fstream>

#include "lacze_do_gnuplota.hh"
#include "Dron.hh"
#include "Scena.hh"
#include "Graniastoslup.hh"
#include "FiguraGeometryczna.hh"
#include "Fabryka.hh"

using namespace std;
/*
void PrzykladZapisuWspolrzednychDoStrumienia(ostream& Strm, Korpus &Pr)
{
  zapis(Strm, Pr);
}

void PrzykladZapisuWspolrzednychDoStrumienia(ostream& Strm, Wirnik &wirnik)
{
  zapiswirnika(Strm, wirnik);
}

void PrzykladZapisuWspolrzednychDoStrumienia(ostream& Strm, Korpus &Pr, Wektor3D W)
{
  for(int i=0; i<8; ++i)
    Pr[i]=Pr[i]+W;

  zapis(Strm, Pr);
}

void PrzykladZapisuWspolrzednychDoStrumienia(ostream& Strm, Korpus &Pr, Macierz3x3 &M)
{
  for(int i=0; i<8; ++i)
    Pr[i]=M*Pr[i];

  zapis(Strm, Pr);
}
*/


/*
Wektor3D W, W_przesuniecia;
  Macierz3x3 M;
  W[0]=0; W[1]=0; W[2]=0;
  double kat_x, kat_y, kat_z;
  Pr.utworz(120, 120, 120);
  W_przesuniecia[0]=-30; W_przesuniecia[1]=-30; W_przesuniecia[2]=-30;
  PrzykladZapisuWspolrzednychDoStrumienia(cout, Pr, W_przesuniecia);
  if(!PrzykladZapisuWspolrzednychDoPliku("prostopadloscian.dat", Pr, W_przesuniecia)) return 1;

  int liczba_powtorzen=0;

  PrzykladZapisuWspolrzednychDoStrumienia(cout, Pr);
  if(!PrzykladZapisuWspolrzednychDoPliku("prostopadloscian.dat",Pr)) return 1;
  Lacze.Rysuj();
  cin.ignore(10000, '\n');
*/


void WyswietlMenu()
{
  cout << "Liczba wszystkich stworzonych obiektów klasy Wektor3D: ";
  cout << Wektor3D::ZwrocLacznaIloscWektorow() << endl;
  cout << "Aktualnia liczba obiektów klasy Wektor3D: ";
  cout << Wektor3D::ZwrocIloscWektorow() << endl;
  cout << "o - obrót" << endl;
  cout << "l - lot na wprost" << endl;
  cout << "c - cofanie" << endl;
  cout << "s - selekcja drona" << endl;
  cout << "w - wyswietl menu" << endl << endl;
  cout << "k - koniec działania programu" << endl;
  cout<<"p- dodaj przeszkode"<<endl;
  cout<<"u- usun przeszkode"<<endl;
  cout<<"d- dodaj drona"<<endl;
  cout<<"X- usun aktywnego drona"<<endl;

  
}


void UstawLacze(PzG::LaczeDoGNUPlota &Lacze)
{
  /*
  std::string NazwaPliku;
  Lacze.DodajNazwePliku("Korpus.dat");
  for (int i = 0; i < LICZBA_ROTOROW; ++i)
  {
    NazwaPliku = "Rotor";
    NazwaPliku.append(std::to_string(i));
    NazwaPliku.append(".dat");
    Lacze.DodajNazwePliku(NazwaPliku.c_str());
  }
*/

  std::string NazwaPliku;

    for(int i = 0; i < LICZBA_DRONOW; i++)
    {
        NazwaPliku = "Korpus";
        NazwaPliku += std::to_string(i); //konwertuje na string 
        NazwaPliku += ".dat";
        Lacze.DodajNazwePliku(NazwaPliku.c_str());  //Zmienia string na char

        for(int j = 0; j < LICZBA_ROTOROW; j++)
        {
            NazwaPliku = "Rotor";
            NazwaPliku += std::to_string(i);
            NazwaPliku += "_";
            NazwaPliku += std::to_string(j);
            NazwaPliku += ".dat";
            Lacze.DodajNazwePliku(NazwaPliku.c_str());
        }
    }

  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.UstawZakresY(-150, 150);
  Lacze.UstawZakresX(-150, 150);
  Lacze.UstawZakresZ(-150, 150);
  Lacze.Inicjalizuj();
}



void IloscObiektow()
{
  std::cout << std::endl << "Laczna ilosc stworzonych obiektów klasy Wektor3D: "<<  Wektor3D::ZwrocLacznaIloscWektorow() << std::endl;
  std::cout << "Ilosc istniejacych obiektow klasy Wektor3D: " << Wektor3D::ZwrocIloscWektorow() << std::endl;
}


void AktualnyDron(Scena* wsk_scena)
{
  cout << endl << "Aktualnie wyselekcjonowanym dronem jest:" << endl;
  cout << "  Dron " << wsk_scena->ZwrocNrDrona()+1 << ".   " << "Wspolrzedne: (" << wsk_scena->ZwrocDrona()->ZwrocSrodek() << ')' << endl << endl;
}



int main()
{
  PzG::LaczeDoGNUPlota Lacze; // Deklaracje zmiennych
  Dron dr;
  Scena scena;
  ObiektSceny obsceny;
  char wybor='w';

 /*
 std::string NazwaPliku;
  Lacze.DodajNazwePliku("Korpus.dat");
  for (int i = 0; i < LICZBA_ROTOROW; ++i)
  {
    NazwaPliku = "Rotor";
    NazwaPliku.append(std::to_string(i));
    NazwaPliku.append(".dat");
    Lacze.DodajNazwePliku(NazwaPliku.c_str());
  }

  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.UstawZakresY(-150, 150);
  Lacze.UstawZakresX(-150, 150);
  Lacze.UstawZakresZ(-150, 150);
  Lacze.Inicjalizuj();
  */

  UstawLacze(Lacze); // Ustawienie parametrow lacza
  scena.Inicjalizuj(Lacze);
  dr.Inicjalizuj(); // Inicjalizacja drona
  dr.Zapisz(0); // Wstepny zapis do pliku
  Lacze.Rysuj();

  IloscObiektow();
  AktualnyDron(&scena);
  WyswietlMenu();
  

  while(wybor!='k')
  {
    std::cin >> wybor;
    

    switch (wybor)
    {
      

      case 'o':
      {
       /* dr.Obrot(Lacze);
        dr.Zapisz();
        Lacze.Rysuj();*/

        AktualnyDron(&scena);
        scena.ZwrocDrona()->Obrot(Lacze, scena.ZwrocNrDrona());


        break;
      }
      

      case 'l':
      {
      
      /*
         Wektor3D P;
      if(kat<0) P[2]=-P[2];
      P[0]=sqrt(pow(droga*cos(kat*M_PI/180),2)+pow(droga*sin(kat*M_PI/180),2))/ilosc_iteracji;
      P[1]=droga*cos(kat*M_PI/180)/10;
      P[2]=droga*sin(kat*M_PI/180)/ilosc_iteracji;

      for(int i=0; i<ilosc_iteracji; ++i)
      {
        PrzykladZapisuWspolrzednychDoStrumienia(cout, Pr, P);
        if(!PrzykladZapisuWspolrzednychDoPliku("prostopadloscian.dat", Pr, P)) return 1;
        Lacze.Rysuj();
        usleep(100000);
      }
      */

       /* dr.Ruch(Lacze);
        dr.Zapisz();
        Lacze.Rysuj();*/
        AktualnyDron(&scena);
        scena.ZwrocDrona()->Ruch(Lacze, scena.ZwrocNrDrona());

        break;
      }

      
      case 'c':
      {

        /*
        Wektor3D C;
      if(kat<0) C[2]=-C[2];
      C[0]=-(sqrt(pow(drogac*cos(katc*M_PI/180),2)+pow(drogac*sin(katc*M_PI/180),2)))/ilosc_iteracji;
      C[1]=-(drogac*cos(katc*M_PI/180))/10;
      C[2]=-(drogac*sin(katc*M_PI/180))/ilosc_iteracji;


      for(int i=0; i<ilosc_iteracji; ++i)
      {
        PrzykladZapisuWspolrzednychDoStrumienia(cout, Pr, C);
        if(!PrzykladZapisuWspolrzednychDoPliku("prostopadloscian.dat", Pr, C)) return 1;
        Lacze.Rysuj();
        usleep(100000);
      }
      */

       /* dr.Cofanie(Lacze);
        dr.Zapisz();
        Lacze.Rysuj(); */
        AktualnyDron(&scena);
        scena.ZwrocDrona()->Cofanie(Lacze, scena.ZwrocNrDrona());
        break;
      }

     
      case 'w':
      {
        WyswietlMenu();
        break;
      }
      
      case 's':
      {
        AktualnyDron(&scena);
        scena.ZmienDrona();
      }

     
      case 'p': 
      {
        scena.DodajPrzeszkode();
        break;
      }

      case 'u': 
      {
        scena.UsunPrzeszkode();
        break;
      }

     case 'd':
     {
       scena.DodajDrona();
       break;
      }
 
      case 'X':
      {
        scena.UsunDrona();
        break;
      }


      case 'k':
      {
        Fabryka::UsunFabryke();
        return 0;
      }
      
/*
      case 'm':
      {
        for (int i = 0; i < 360; i++) dr.Animacja(Lacze);
      }
*/

    default:
    {
      std::cout << "Nie ma takiej opcji, spróbuj jeszcze raz." << std::endl;
    }

    }
  }
  return 0;
}
