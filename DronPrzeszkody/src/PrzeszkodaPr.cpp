#include "PrzeszkodaPr.hh"

void PrzeszkodaPr::Inicjalizuj(double dlug, double szer, double wys)
{
    wys /= 8;
    szer /= 10;
    dlug /= 8;


    Punkt.push_back(Wektor3D(dlug,-szer,wys));
    Punkt.push_back(Wektor3D(dlug,-szer,wys));
    Punkt.push_back(Wektor3D(dlug,szer,wys));
    Punkt.push_back(Wektor3D(dlug,szer,wys));

    Punkt.push_back(Wektor3D(dlug,-szer,wys));
    Punkt.push_back(Wektor3D(dlug,-szer,-wys));
    Punkt.push_back(Wektor3D(dlug,szer,-wys));
    Punkt.push_back(Wektor3D(dlug,szer,wys));

    Punkt.push_back(Wektor3D(dlug,-szer,wys));
    Punkt.push_back(Wektor3D(-dlug,-szer,-wys));
    Punkt.push_back(Wektor3D(-dlug,szer,-wys));
    Punkt.push_back(Wektor3D(dlug,szer,wys));

    Punkt.push_back(Wektor3D(dlug,-szer,wys));
    Punkt.push_back(Wektor3D(-dlug,-szer,wys));
    Punkt.push_back(Wektor3D(-dlug,szer,wys));
    Punkt.push_back(Wektor3D(dlug,szer,wys));

    Punkt.push_back(Wektor3D(dlug,-szer,wys));
    Punkt.push_back(Wektor3D(dlug,-szer,wys));
    Punkt.push_back(Wektor3D(dlug,szer,wys));
    Punkt.push_back(Wektor3D(dlug,szer,wys));

    Punkt_globalny = Punkt;
}

