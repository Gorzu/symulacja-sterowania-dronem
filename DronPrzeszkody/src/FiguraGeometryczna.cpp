#include "FiguraGeometryczna.hh"

void FiguraGeometryczna::Zapis(std::ostream &Strm)
{
    for(long unsigned int i = 0; i < Punkt_globalny.size(); i++)
    {
        if(i % 4 == 0)
            Strm << std::endl;

        for(int j = 0; j < 3; j++)
            Strm << std::setw(25) << std::fixed << std::setprecision(10) << Punkt_globalny[i][j];

        Strm << std::endl;
    }
}

void FiguraGeometryczna::Przesun(Wektor3D wektor)
{
    for(long unsigned int i = 0; i < Punkt_globalny.size(); i++)
        Punkt_globalny[i] += wektor;

    Wektor_Trans += wektor;
}

void FiguraGeometryczna::Obrot(double kat)
{
    Macierz3x3 macRot;
    macRot.kat_z(kat); 
    katObrotu += kat;

    for(unsigned int i = 0; i < Punkt_globalny.size(); i++)
    {
        Punkt[i] = macRot * Punkt[i];
        Punkt_globalny[i] = Punkt[i] + Wektor_Trans;
    }
}
