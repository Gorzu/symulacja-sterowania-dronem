#include "Graniastoslup.hh"
#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;

void Graniastoslup::Inicjalizuj()
{
 
    Punkt.push_back(Wektor3D(0,0,10));
    Punkt.push_back(Wektor3D(10,0,10));
    Punkt.push_back(Wektor3D(10,0,-10));
    Punkt.push_back(Wektor3D(0,0,-10));

    Punkt.push_back(Wektor3D(0,0,10));
    Punkt.push_back(Wektor3D(5,10,10));
    Punkt.push_back(Wektor3D(5,10,-10));
    Punkt.push_back(Wektor3D(0,0,-10));

    Punkt.push_back(Wektor3D(0,0,10));
    Punkt.push_back(Wektor3D(-5,10,10));
    Punkt.push_back(Wektor3D(-5,10,-10));
    Punkt.push_back(Wektor3D(0,0,-10));

    Punkt.push_back(Wektor3D(0,0,10));
    Punkt.push_back(Wektor3D(-10,0,10));
    Punkt.push_back(Wektor3D(-10,0,-10));
    Punkt.push_back(Wektor3D(0,0,-10));

    Punkt.push_back(Wektor3D(0,0,10));
    Punkt.push_back(Wektor3D(-5,-10,10));
    Punkt.push_back(Wektor3D(-5,-10,-10));
    Punkt.push_back(Wektor3D(0,0,-10));

    Punkt.push_back(Wektor3D(0,0,10));
    Punkt.push_back(Wektor3D(5,-10,10));
    Punkt.push_back(Wektor3D(5,-10,-10));
    Punkt.push_back(Wektor3D(0,0,-10));

    Punkt.push_back(Wektor3D(0,0,10));
    Punkt.push_back(Wektor3D(10,0,10));
    Punkt.push_back(Wektor3D(10,0,-10));
    Punkt.push_back(Wektor3D(0,0,-10));


  /*
  Punkt.push_back(Wektor3D(2, 2, 2));
  Punkt.push_back(Wektor3D(-2, 2, 2));
  Punkt.push_back(Wektor3D(3, 0, 2));
  Punkt.push_back(Wektor3D(-3, 0, 2));
  Punkt.push_back(Wektor3D(2, -2, 2));
  Punkt.push_back(Wektor3D(-2, -2, 2));

  Punkt.push_back(Wektor3D(2, -2, -2));
  Punkt.push_back(Wektor3D(-2, -2, -2));
  Punkt.push_back(Wektor3D(3, 0, -2));
  Punkt.push_back(Wektor3D(-3, 0, -2));
  Punkt.push_back(Wektor3D(2, 2, -2));
  Punkt.push_back(Wektor3D(-2, 2, -2));
*/

  Punkt_globalny = Punkt;
}


/*
void Graniastoslup::ObrotSmigiel(double kat)
{
  Macierz<3> Mac_Obrotu = MacierzZ(kat);
  for (long unsigned int i = 0; i < Punkt.size(); ++i)
  {
    this->Punkt[i] = this->Punkt[i] - this->srodek_drona;
    this->Punkt[i] = Mac_Obrotu * this->Punkt[i];
    this->Punkt[i] = this->Punkt[i] + this->srodek_drona;
    Punkt_globalny[i] = Punkt[i] + Wektor_Trans;
  }
}
*/


void Graniastoslup::ObrotSmigiel(double kat)
{
  Macierz3x3 Mac_Obrotu;
  Mac_Obrotu.kat_z(kat);

  for (long unsigned int i = 0; i < Punkt.size(); i++)
  {
    Punkt[i] = Punkt[i] - srodek_drona;
    Punkt[i] = Mac_Obrotu * Punkt[i];
    Punkt[i] = Punkt[i] + srodek_drona;
    Punkt_globalny[i] = Punkt[i] + Wektor_Trans;
  }
}


void Graniastoslup::UstawSrodekDrona(Wektor3D Arg)
{
  srodek_drona = Arg;
  srodek_drona[2] += 1;
}


void Graniastoslup::UstawPolozenie(Wektor3D offset)
{
  for (long unsigned int i = 0; i < Punkt.size(); ++i)
  {
    Punkt[i] += offset;
  }
  Punkt_globalny = Punkt;
  //this->srodek_drona = srodek_drona;
}


Graniastoslup Graniastoslup::operator+(Wektor3D Arg2)
{
  for (long unsigned int i = 0; i < Punkt.size(); ++i)
  {
    Punkt_globalny[i] += Arg2;
  }
  Wektor_Trans = Wektor_Trans + Arg2;
  return (*this);
}