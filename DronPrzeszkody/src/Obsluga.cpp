#include "Obsluga.hh"

using namespace std;


void WyswietlMenu()
{
  cout << "Liczba wszystkich stworzonych obiektów klasy Wektor3D: ";
  cout << Wektor3D::ZwrocLacznaIloscWektorow() << endl;
  cout << "Aktualnia liczba obiektów klasy Wektor3D: ";
  cout << Wektor3D::ZwrocIloscWektorow() << endl;
  cout << "o - obrót" << endl;
  cout << "l - lot na wprost" << endl;
  cout << "c - cofanie" << endl;
  cout << "s - selekcja drona" << endl;
  cout << "w - wyswietl menu" << endl << endl;
  cout << "k - koniec działania programu" << endl;
  
}


void UstawLacze(PzG::LaczeDoGNUPlota &Lacze)
{
  std::string NazwaPliku;
  Lacze.DodajNazwePliku("Korpus.dat");
  for (int i = 0; i < LICZBA_ROTOROW; ++i)
  {
    NazwaPliku = "Rotor";
    NazwaPliku.append(std::to_string(i));
    NazwaPliku.append(".dat");
    Lacze.DodajNazwePliku(NazwaPliku.c_str());
  }

  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.UstawZakresY(-150, 150);
  Lacze.UstawZakresX(-150, 150);
  Lacze.UstawZakresZ(-150, 150);
  Lacze.Inicjalizuj();
}



