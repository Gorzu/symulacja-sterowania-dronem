#include "Fabryka.hh"

Fabryka *Fabryka::_wFabryka = nullptr;



std::shared_ptr<ObiektSceny> Fabryka::DodajPrzszkode(double X,double Y, double Z, Wektor3D Przesunieci, int numerobiektu) const
{
        std::shared_ptr<PrzeszkodaPr> Prz;
        Prz = std::make_shared<PrzeszkodaPr>();
        Prz->Incjalizuj2(numerobiektu,X,Y,Z);
        Prz->Przesun(Przesunieci);
        Prz->zapisz();
        return Prz;

}



std::shared_ptr<ObiektSceny> Fabryka::Stworz(TYP_OBIEKTU TYP, int numerobiektu, Wektor3D Przesuniecie)
{
        switch (TYP)
        {
            case DRON:
                {
                    std::shared_ptr<Dron> Dr;
                    Dr->Incjalizuj(numerobiektu);
                    Dr->Przesun(Przesunieci);
                    Dr->zapisz();
                    return Dr;

                }
            case PRZESZKODA:
                {
                    std::shared_ptr<PrzeszkodaPr> Prz;
                    Prz->Incjalizuj(numerobiektu);
                    Prz->Przesun(Przesunieci);
                    Prz->zapisz();
                    return Prz;
                }           

        }
}
