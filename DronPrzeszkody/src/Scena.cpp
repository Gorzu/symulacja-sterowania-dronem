#include "Scena.hh"
#include <cstdlib>
#include <ctime>


int ilosc_przeszkod=0;
int ilosc_dronow=0;



void Scena::Inicjalizuj(PzG::LaczeDoGNUPlota& Lacze)
{
    srand( time( NULL ) );

    for(int i = 0; i < LICZBA_DRONOW; i++)
    {
        std::shared_ptr<Dron> dronik = std::make_shared<Dron>(); //wskazuje na nową instancje obiektu klasy dron
        //Dron *wsk_dron = new Dron;
        listaDronow.push_back(dronik);///dronik wskaźnik na obiekt
        listaObiekt.push_back(dronik);
        dronik->Inicjalizuj();
        dronik->Ustaw(Lacze, Wektor3D(i*80,i*(-50),i*80), i);
        dronik->Zapisz(i);
    }

    for(int i = 0; i < LICZBA_PRZESZKOD; i++)
    {
        std::string NazwaPliku;
        std::fstream PlikWy;

            NazwaPliku = "Przeszkoda";
            NazwaPliku += std::to_string(i); //konwertuje na string 
            NazwaPliku += ".dat";
            Lacze.DodajNazwePliku(NazwaPliku.c_str()); //sting na char

         
    }

            std::fstream PlikWy;

    // I
            PlikWy.open("Przeszkoda0.dat", std::fstream::out); //jeżeli plik jest stworzony to go zrobi   
            std::shared_ptr<PrzeszkodaPr> przeszkoda = std::make_shared<PrzeszkodaPr>(); //wskazuje na nową instancje obiektu klasy dron
            listaObiekt.push_back(przeszkoda);
            przeszkoda->Inicjalizuj(190,140,140);
            przeszkoda->Przesun(Wektor3D(-90,100,90)); 
            przeszkoda->Zapis(PlikWy);
            PlikWy.close();

    // II
            PlikWy.open("Przeszkoda1.dat", std::fstream::out); //jeżeli plik jest stworzony to go zrobi   
            przeszkoda = std::make_shared<PrzeszkodaPr>(); //wskazuje na nową instancje obiektu klasy dron
            listaObiekt.push_back(przeszkoda);
            przeszkoda->Inicjalizuj(200,120,200);
            przeszkoda->Przesun(Wektor3D(90,40,65));
            przeszkoda->Zapis(PlikWy);
            PlikWy.close();

    // III
            PlikWy.open("Przeszkoda2.dat", std::fstream::out); //jeżeli plik jest stworzony to go zrobi   
            przeszkoda = std::make_shared<PrzeszkodaPr>(); //wskazuje na nową instancje obiektu klasy dron
            listaObiekt.push_back(przeszkoda);
            przeszkoda->Inicjalizuj(180,180,100);
            przeszkoda->Przesun(Wektor3D(80,-130, -100));
            przeszkoda->Zapis(PlikWy);
            PlikWy.close();

    aktualny = listaDronow.begin()->get();
    indeksWybranego=0;


/*!
*\brief Tworzenie fabryki.
*/

    Fabryka::StworzFabryke();//Tworzenie Fabryki

    int t=3;
    for(int i = 0; i < 3;++i)
    {
     double tab[]={40*t*pow(-1,t+1),40*t*pow(-1,2*t),40*t*pow(-1,t+1)};
     Wektor3D y(tab); 
     obiekty.push_back(Fabryka::UzyjFabryke()->Stworz(PRZESZKODA,i,y));
     ++t;
     ++ilosc_przeszkod;
    }

int ta=0;
for(int i = 3; i < 6; ++i)
{
  double tab[]={40*ta*pow(-1,ta+1),40*ta*pow(-1,2*ta),40*ta*pow(-1,ta+1)};
  Wektor3D y(tab); 
  std::shared_ptr<ObiektSceny> Pa;
  Pa=Fabryka::UzyjFabryke()->Stworz(DRON,i,y);
  drony.push_back(std::static_pointer_cast<Dron>(Pa));
  obiekty.push_back(Pa);
  ilosc_dronow++;
  ++ta;
}

} // koniec Scena::Inicjalizuj


Dron* Scena::ZwrocDrona() //*Dron - wskaźnik na drona 
{
    return aktualny;  //zwraca element wybrany z listy w zmiennej drona
}


void Scena::ZmienDrona()
{
    int wybor;

    WyswietlDrony();
    std::cout << std::endl << "Wprowadz numer drona lub 0: ";
    std::cin >> wybor;


///////////////////////////////////////////////////////////////////////////////////////////////////

    if(wybor > 0 )
    {
        if(wybor > std::distance(listaDronow.begin(), ilosc_dronow.end()))  
            std::cout << "Nie ma takiego drona" << std::endl;
        else
        {
            std::list<std::shared_ptr<Dron>>::iterator It; // stworzenie iteratora It
            It = listaDronow.begin(); //przypisanie do niego pierwszego elementu
            std::advance(It, wybor - 1);
            aktualny=It->get(); //zwraca wskaźnik iteratora

            indeksWybranego = wybor-1;
        }
    }
}

void Scena::WyswietlDrony()
{
    int i = 1;

    std::cout << "      0. - zaniechaj zmiany selekcji" << std::endl << std::endl;

    for(std::shared_ptr<Dron> dron : ilosc_dronow) 
    {
        std::cout << "  Dron " << i << ".   "<< "Wspolrzedne: (" << ZwrocDrona()->ZwrocSrodek() << ')' <<  std::endl;
        i++;
    }

    std::cout << std::endl << "Podaj numer drona, dla ktorego maja być wykonane operacje sterowania" << std::endl;
}


int Scena::ZwrocNrDrona()
{
    return indeksWybranego;
}




void Scena::DodajPrzeszkode()
{
std::string NazwaPliku;

double X,Y,Z;
Wektor3D trans;
do
{
std::cin.clear();
std::cin.ignore();
std::cout<<"Podaj odległość w kolejnych osiach X Y Z:__\b\b";
std::cin>>X;
std::cin>>Y;
std::cin>>Z;
if (!std::cin.good())
{
    std::cerr<<"Wystąpił błąd. Spróbuj jeszcze raz"<<std::endl;
}

} while(!std::cin.good());

do
{
std::cin.clear();
std::cin.ignore();
std::cout<<"Podaj docelowe miejsce przeszkody w kolejnych osiach X Y Z:__\b\b";
std::cin>>trans;
if (!std::cin.good())
{
    std::cerr<<"Wystąpił błąd. Spróbuj jeszcze raz."<<std::endl;
}   

} while(!std::cin.good());
 ++ilosc_przeszkod;
obiekty.push_front(Fabryka::UzyjFabryke()->DodajPrzszkode(X,Y,Z,trans,ilosc_przeszkod));
   
    NazwaPliku = "Przeszkoda";
    NazwaPliku.append(std::to_string(ilosc_przeszkod));
    NazwaPliku.append(".dat");
    Lacze.DodajNazwePliku(NazwaPliku.c_str(),PzG::SR_Ciagly);
    Lacze.Rysuj();
}



void Scena::UsunPrzeszkode()
{   
    std::string NazwaPliku;
   
    itobiekt=obiekty.begin();
    Lacze.UsunNazwePliku(ZwrocObiekt()->WezNazwe().c_str());
    obiekty.erase(itobiekt);
    itobiekt=obiekty.begin();
    
    Lacze.Rysuj();
    --ilosc_przeszkod;
}


void Scena::UsunDrona()
{


for(unsigned int i = 0; i < (obiekty.size()-drony.size())+Indeks(); i++)
{
    ++itobiekt;
}

if(drony.size()<2)
{
    std::cout<<"Zostal tylko jeden dron, operacja przerwana."<<std::endl;
}
else
{

    for(int i = 0; i <5; ++i)
    {
        Lacze.UsunNazwePliku(ZwrocDrona()->ZwrocNazwy(i).c_str());
    }

    drony.erase(itdron);
    obiekty.erase(itobiekt);
    itobiekt=obiekty.begin();
    itdron=drony.begin();

}
Lacze.Rysuj();

}


/*
void Scena::DodajDrona()
{
    std::string NazwaPliku;    
    Wektor3D trans;
    int x=ilosc_dronow+4;

    NazwaPliku = "Korpus";
    NazwaPliku.append(std::to_string(x));
    NazwaPliku.append(".dat");
    Lacze.DodajNazwePliku(NazwaPliku.c_str());

    for (int j = 1; j < 5; ++j) 
    {
      NazwaPliku = "Rotor";
      NazwaPliku.append(std::to_string(x));
      NazwaPliku.append("_");
      NazwaPliku.append(std::to_string(j));
      NazwaPliku.append(".dat");  
      Lacze.DodajNazwePliku(NazwaPliku.c_str());
    }
Lacze.Rysuj();

} */
