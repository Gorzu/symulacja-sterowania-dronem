#include "Dron.hh"


void Dron::Ruch(PzG::LaczeDoGNUPlota Lacze, int numerDrona)
{
  Wektor3D ruch_uz, ruch_kon;
  double kat;
  int ilosc_klatek;
  std::cout << "Wprowadź drogę, jaką ma przelecieć dron: ";
  std::cin >> ruch_uz[0];
  ilosc_klatek = abs(ruch_uz[0])*4;
  std::cout << "Podaj kąt wznoszenia (lub opadania): ";
  std::cin >> kat;
  ruch_kon[0] = ruch_uz[0] * cos(Kat_Obrotu * M_PI / 180);
  ruch_kon[1] = ruch_uz[0] * sin(Kat_Obrotu * M_PI / 180);
  ruch_kon[2] = tan(kat * M_PI / 180) * abs(ruch_uz[0]);
  for (int klatka = 0; klatka < ilosc_klatek; ++klatka)
  {
    Korpus.Przesun(ruch_kon / ilosc_klatek);
    for (int i = 0; i < LICZBA_ROTOROW; ++i)
    {
      Rotor[i].Przesun(ruch_kon / ilosc_klatek);
    }
    this->Animacja(Lacze, numerDrona);
    this->Zapisz(numerDrona);
    Lacze.Rysuj();
    usleep(20000);
  }
}



void Dron::Cofanie(PzG::LaczeDoGNUPlota Lacze, int numerDrona)
{
  Wektor3D Cruch_uz, Cruch_kon;
  double Ckat;
  int Cilosc_klatek;
  std::cout << "Wprowadź drogę, o jaką dron ma się cofnąć: ";
  std::cin >> Cruch_uz[0];
  Cilosc_klatek = abs(Cruch_uz[0])*4;
  std::cout << "Podak kąt wznoszenia (lub opadania): ";
  std::cin >> Ckat;
  Cruch_kon[0] = -Cruch_uz[0] * cos(Kat_Obrotu * M_PI / 180);
  Cruch_kon[1] = -Cruch_uz[0] * sin(Kat_Obrotu * M_PI / 180);
  Cruch_kon[2] = -tan(Ckat * M_PI / 180) * abs(Cruch_uz[0]);
  
  Korpus.Przesun(Cruch_kon / Cilosc_klatek);
  
for (int klatka = 0; klatka < Cilosc_klatek; ++klatka)
  {
    Korpus.Przesun(Cruch_kon / Cilosc_klatek);
    for (int i = 0; i < LICZBA_ROTOROW; ++i)
    {
      Rotor[i].Przesun(Cruch_kon / Cilosc_klatek);
    }

    this->Animacja(Lacze, numerDrona);
    this->Zapisz(numerDrona);
    Lacze.Rysuj();
    usleep(20000);
  }
}



void Dron::Obrot(PzG::LaczeDoGNUPlota Lacze, int numerDrona)
{
  double kat;
  std::cout << "Podaj kąt, o jaki dron ma się obrócić: ";
  std::cin >> kat;
  Kat_Obrotu += kat;
  if (Kat_Obrotu >= 360)
    Kat_Obrotu -= 360;
  else if (Kat_Obrotu <= -360)
    Kat_Obrotu += 360;

  for (int klatka = 0; klatka < kat; ++klatka)
  {
    Korpus.Obrot(1);
    for (int i = 0; i < LICZBA_ROTOROW; ++i)
    {
      Rotor[i].Obrot(1);
    }
    this->Animacja(Lacze, numerDrona);
    this->Zapisz(numerDrona);
    Lacze.Rysuj();
    usleep(20000);
  }
}


void Dron::Zapisz(int numerDrona)
{
  
  std::string NazwaPliku;
  std::fstream PlikWy;

  NazwaPliku = "Korpus";
  NazwaPliku += std::to_string(numerDrona); //konwertuje na string 
  NazwaPliku += ".dat";

  PlikWy.open(NazwaPliku, std::fstream::out); //jeżeli plik jest stworzony to go zrobi
  Korpus.Zapis(PlikWy);
  PlikWy.close();


  for (int i = 0; i < LICZBA_ROTOROW; i++)
  {
    /*
    NazwaPliku = "Rotor";
    NazwaPliku.append(std::to_string(i));
    NazwaPliku.append(".dat");
    PlikWy.open(NazwaPliku, std::fstream::out);
    */

    NazwaPliku = "Rotor";
    NazwaPliku += std::to_string(numerDrona);
    NazwaPliku += "_";
    NazwaPliku += std::to_string(i);
    NazwaPliku += ".dat";
    PlikWy.open(NazwaPliku, std::fstream::out);


    Rotor[i].Zapis(PlikWy);
    PlikWy.close();
  }
}


void Dron::Inicjalizuj()
{
  Korpus.Inicjalizuj();
  for (int i = 0; i < LICZBA_ROTOROW; i++)
  {
    Rotor[i].Inicjalizuj();
  }
  Rotor[0].UstawPolozenie(Wektor3D(20, 20, 27));
  Rotor[1].UstawPolozenie(Wektor3D(20, -20, 27));
  Rotor[2].UstawPolozenie(Wektor3D(-20, 20, 27));
  Rotor[3].UstawPolozenie(Wektor3D(-20, -20, 27));

}

void Dron::Animacja(PzG::LaczeDoGNUPlota Lacze, int numerDrona)
{
  AktualizujRotory();
  Rotor[0].ObrotSmigiel(10);
  Rotor[1].ObrotSmigiel(-10);
  Rotor[2].ObrotSmigiel(-10);
  Rotor[3].ObrotSmigiel(10);

  this->Zapisz(numerDrona);
  Lacze.Rysuj();
  usleep(1000);
}


/*
void Dron::AktualizujRotory()
{
  for (int i = 0; i < LICZBA_ROTOROW; ++i)
  {
    Rotor[i].UstawSrodekDrona(Korpus[i]);
  }
}
*/


void Dron::AktualizujRotory()
{
    Rotor[0].UstawSrodekDrona(Korpus[2]);
    Rotor[1].UstawSrodekDrona(Korpus[1]);
    Rotor[2].UstawSrodekDrona(Korpus[14]);
    Rotor[3].UstawSrodekDrona(Korpus[13]);
}


void Dron::Ustaw(PzG::LaczeDoGNUPlota& Lacze, Wektor3D wek, int numerDrona)
{
  Korpus.Przesun(wek);

  for(int j = 0; j < LICZBA_ROTOROW; j++)
  {
    Rotor[j].Przesun(wek);
  }
  
  this-> Animacja(Lacze, numerDrona);
  this-> Zapisz(numerDrona);
  Lacze.Rysuj();

  srodek += wek;
}
